// fetch() method in JavaScript is used to send request in the server and load the received response in the webpage. The request and response is in JSON format.

// Syntax:
	// fetch('url', options)
			// url - this is the url which the request is to be made (endpoint).
			// options - array of properties that contains the HTTP method, body of the request, headers.

// Get post data.
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
//invoke the showPost
.then((data) => showPost(data));

//Add post data.

		
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	//Prevent the page from loading
	e.preventDefault();
	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Successfully Added!");
	})

	// resets the state of our input into blanks after submitting a new posot.

	document.querySelector("#txt-title").value = null;
	document.querySelector("#txt-body").value = null;

})

// View Posts
const showPost = (posts) =>{
	let postEntries = "";

	// We will used forEach() to display each movie inside our mock database.
	posts.forEach((post) =>{
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	// console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit Post Button

const editPost = (id) =>{
	// Contain the value of the title and body in a variabloe
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Pass the id, title, and body of the movie post to be updated in the Edit Post/Form.
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	// To removed the disable property
	document.querySelector("#btn-submit-update").removeAttribute("disabled");
}

// Update Post
document.querySelector("#form-edit-post").addEventListener("submit", (e) =>{
	e.preventDefault();
	let id = document.querySelector("#txt-edit-id").value

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: "PUT",
		body: JSON.stringify({
			id: id,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	})
	.then(response => response.json())
	.then((data) =>{
		console.log(data);
		alert("Successfully Updated!")

		// resets the state of our input into blanks after submitting a new posot.

		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;

		// Add a attribute in a HTML element
		document.querySelector("#btn-submit-update").setAttribute("disabled", true)
	})

})

//Activity Delete a post
const deletePost = (id) => {
	// filter method will save the posts that are not equal to the id parameter.
	posts = posts.filter((post) => {
		if(post.id != id){
			return post;
		}
	})

	console.log(posts);

	// .remove() method removes an element (or node) from the document.
	document.querySelector(`#post-${id}`).remove();
}



